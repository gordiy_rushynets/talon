//
//  main.cpp
//  talon
//
//  Created by Gordiy Rushynets on 2/26/19.
//  Copyright © 2019 Gordiy Rushynets. All rights reserved.
//

#include <iostream>
#include <string>
#include <fstream>

using namespace std;

class Talon
{
private:
    string subjectName;
    string lectureSurname;
    double points;
public:
    Talon() :subjectName(" "), lectureSurname(" "), points(0){}
    Talon(string subName, string lecSur, double point)
    {
        subjectName = subName;
        lectureSurname = lecSur;
        points = point;
        
    }
    Talon(Talon &t)
    {
        subjectName = t.subjectName;
        points = t.points;
        lectureSurname = t.lectureSurname;
        
    }
    
    friend ostream& operator<<(ostream& out, Talon &t)
    {
        out << t.subjectName << " " << t.lectureSurname << " " << t.points << endl;
        return out;
    }
    
    friend istream& operator>>(istream& in, Talon &t)
    {
        in >> t.subjectName >> t.lectureSurname >> t.points;
        return in;
    }
    
    string GetSubjectName()
    {
        return subjectName;
    }
    string GetLectureSurname()
    {
        return lectureSurname;
    }
    
    int GetPoints()
    {
        return points;
    }
    
    void SetSubjectName(string subName)
    {
        subjectName = subName;
    }
    
    void SetLectureSurname(string lecSur)
    {
        lectureSurname = lecSur;
    }
    void SetPoints(double point)
    {
        points = point;
    }
};

class Student
{
protected:
    string surname;
    string group;
    int count;
    
    Talon *talons;
public:
    void set_name(string set_name)
    {
        surname = set_name;
    }
    
    void set_group(string new_group)
    {
        group = new_group;
    }
    
    void set_count(int new_count)
    {
        count = new_count;
    }
    
    Talon &operator[](int index)
    {
        return talons[index];
    }
    
    friend istream &operator >> (istream &in, Student &student)
    {
        return in >> student.surname >> student.group >> student.count;
    }
    
    ~Student()
    {
        delete [] talons;
    }
};

int main(int argc, const char * argv[]) {
    int count_students;
    ifstream fin_student("/Users/gordiy/cpp/talon/talon/student.txt");
    
    fin_student >> count_students;
    
    Student *students = new Student[count_students];
    
    
    for(int i = 0; i < count_students; ++i)
    {
        string name;
        fin_student >> name;
        students[i].set_name(name);
        
        string group;
        fin_student >> group;
        students[i].set_group(group);
        
        int count_talons;
        fin_student >> count_talons;
        students[i].set_count(count_talons);
        
        for(int j = 0; j < count_talons; j++)
        {
            string subject;
            fin_student >> subject;
            students[i][j].SetSubjectName(subject);
            
            double grade;
            fin_student >> grade;
            students[i][j].SetPoints(grade);
            
            string lector;
            fin_student >> lector;
            students[i][j].SetLectureSurname(lector);
        }
        
    }
    
    return 0;
}
